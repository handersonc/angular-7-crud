import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { UserModel } from '../UserModel';
import { UserService } from '../user.service';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

  users: UserModel[];

  constructor(private userService: UserService,
    private router: Router) { }

  ngOnInit() {
    this.getAllUsers();
  }

  getAllUsers(): void {
    this.userService.getAllUsers().subscribe(data=>{
      this.users = data;
    });
  };

  addUser(): void {
    this.router.navigate(['add-user']);
  }

  deleteUser(user: UserModel){
    
    this.userService.deleteUser(user._id).subscribe(data=>{
      console.log(data);
      this.getAllUsers();
    });
  }

  updateUser(user: UserModel){
    localStorage.removeItem("userId");
    localStorage.setItem("userId", user._id);
    this.router.navigate(['edit-user']);
  }

}
