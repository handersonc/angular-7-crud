import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserModel } from './UserModel';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
    private cookieService: CookieService) { }

  baseurl: string = "http://localhost:3000/api/v1/";

  getAllUsers(){
    return this.http.get<UserModel[]>(this.baseurl + 'users', {
      headers: {
        'Authorization': `Bearer ${this.cookieService.get('jwt')}`
      }
    });
  }

  getUserById(id: string){
    return this.http.get<UserModel>(
      this.baseurl + 'users' + '/' + id,{
        headers: {
          'Authorization': `Bearer ${this.cookieService.get('jwt')}`
        }
    });
  }

  addUser(user: UserModel){
    return this.http.post(
      this.baseurl + 'users', user,
      {
        headers: {
          'Authorization': `Bearer ${this.cookieService.get('jwt')}`
        }
    });
  }

  deleteUser(id: string){
    return this.http.delete(
      this.baseurl + 'users' + '/' + id, {
      headers: {
        'Authorization': `Bearer ${this.cookieService.get('jwt')}`
      }
    });
  }

  updateUser(user: UserModel){
    return this.http.put(
      this.baseurl + 'users' + '/' + user._id,
      user,
      {
        headers: {
          'Authorization': `Bearer ${this.cookieService.get('jwt')}`
        }
    });
  }
}
