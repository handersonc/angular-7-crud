import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProductModel } from './ProductModel';

@Injectable({
  providedIn: 'root'
})
export class SigninService {

  constructor(private http: HttpClient) { }

  baseurl: string = "http://localhost:3000/api/v1";

  signin(email: string, password: string){
    return this.http.post(`${this.baseurl}/signin`, { 
        email: email, password: password 
    });
  }
}
