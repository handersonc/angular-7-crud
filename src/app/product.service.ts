import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProductModel } from './ProductModel';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private http: HttpClient,
    private cookieService: CookieService) { }

  baseurl: string = "http://localhost:3000/api/v1/";

  getAllProducts(){
    return this.http.get<ProductModel[]>(this.baseurl + 'products', {
      headers: {
        'Authorization': `Bearer ${this.cookieService.get('jwt')}`
      }
    });
  }

  getProductById(id: string){
    return this.http.get<ProductModel>(
      this.baseurl + 'products' + '/' + id,{
        headers: {
          'Authorization': `Bearer ${this.cookieService.get('jwt')}`
        }
    });
  }

  addProduct(product: ProductModel){
    return this.http.post(
      this.baseurl + 'products', product,
      {
        headers: {
          'Authorization': `Bearer ${this.cookieService.get('jwt')}`
        }
    });
  }

  deleteProduct(id: string){
    return this.http.delete(
      this.baseurl + 'products' + '/' + id, {
      headers: {
        'Authorization': `Bearer ${this.cookieService.get('jwt')}`
      }
    });
  }

  updateProduct(product: ProductModel){
    return this.http.put(
      this.baseurl + 'products' + '/' + product._id,
      product,
      {
        headers: {
          'Authorization': `Bearer ${this.cookieService.get('jwt')}`
        }
    });
  }
}
