export class UserModel {
    _id: string;
    firstName: String;
    lastName: String;
    email: String;
    role: String;
    password: String;
}