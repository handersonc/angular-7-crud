import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {  SigninService} from '../signin.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private signinService: SigninService,
    private cookieService: CookieService,
    private router: Router) { }

  ngOnInit() {
    const jwtCookie = this.cookieService.get('jwt');
    if(jwtCookie)
      this.router.navigate(['home']);
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    }, { });
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }

    const credentials = JSON.stringify(this.loginForm.value);
    
    this.signinService.signin(
      this.loginForm.get('email').value, this.loginForm.get('password').value).subscribe((data: any) => {
        console.log(data);
        if(data.status === 401) {
          this.loginForm.controls['email'].setErrors({'incorrect': true});
          this.loginForm.controls['password'].setErrors({'incorrect': true});
          return;
        }else{
          this.cookieService.set('jwt', data.token);
          this.router.navigate(['home'])
        }
      });
  }

}
