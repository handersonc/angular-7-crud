import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";
import { UserService } from '../user.service';
import { Router } from "@angular/router";


@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService
  ) { }

  addForm: FormGroup;
  submitted = false;


  ngOnInit() {
    this.addForm = this.formBuilder.group({
      _id: [],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      role: ['user', Validators.required],
      password: ['', Validators.required],
    });
  }

  onSubmit(){
    this.submitted = true;
    
    if(this.addForm.valid){
      this.userService.addUser(this.addForm.value)
      .subscribe( data => {
        console.log(data);
        this.router.navigate(['list-user']);
      });
    }
  }

  get f() { return this.addForm.controls; }

}
