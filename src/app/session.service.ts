import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as jwt_decode_temp from 'jwt-decode';
import { CookieService } from 'ngx-cookie-service';
const jwt_decode = jwt_decode_temp;

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor(
    private http: HttpClient,
    private cookieService: CookieService) {
  }

  getToken(): string {
    return this.cookieService.get('jwt');
  }

  getTokenPayload() {
      try {
        return jwt_decode(this.getToken());
      } catch (Error) {
        console.error('Catch', Error);
        return null;
      }
  }


  exists(): boolean {
    const tokenInfo = this.getTokenPayload();
    const token = this.getToken();

    if (!token || !tokenInfo) {
        return false;
    }

    return 'userId' in tokenInfo ? true : false;
  }

  remove(): boolean {
    this.cookieService.delete('jwt');
    return true;
  }
}
