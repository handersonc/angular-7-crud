import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";
import { UserService } from '../user.service';
import { Router } from "@angular/router";
import { UserModel } from '../UserModel';


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  product: UserModel;
  editForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService) { }

  ngOnInit() {
    let userId = localStorage.getItem("userId");
    if(!userId){
      alert("Something wrong!");
      this.router.navigate(['user']);
      return;
    }

    this.editForm = this.formBuilder.group({
      _id: [],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      role: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.userService.getUserById(userId).subscribe(data=>{
      console.log(data);
      this.editForm.patchValue(data); //Don't use editForm.setValue() as it will throw console error
    });
  }

  get f() { return this.editForm.controls; }

  onSubmit(){
    this.submitted = true;
    
    if(this.editForm.valid){
      this.userService.updateUser(this.editForm.value)
      .subscribe( data => {
        console.log(data);
        this.router.navigate(['list-user']);
      });
    }
  }

}
