import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { Router } from "@angular/router";
import { SessionService } from './session.service';
import { UserModel } from './UserModel';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Product Webapp';
  sessionActive = null;

  constructor(private sessionService: SessionService,
    private router: Router) { }

  ngOnInit() {
    if (this.sessionService.exists())
      this.sessionActive = this.sessionService.getTokenPayload();
  }
}
