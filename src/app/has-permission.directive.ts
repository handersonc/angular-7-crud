import { Directive, Input, ElementRef, TemplateRef, ViewContainerRef } from '@angular/core';
import { SessionService } from './session.service';

@Directive({
  selector: '[hasPermission]'
})
export class HasPermissionDirective {
  private permissions = [];
  private user = null;

  constructor(
    private element: ElementRef,
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private sessionService: SessionService
  ) { }

  @Input()
  set hasPermission(val) {
    this.permissions = val;
    this.user = this.sessionService.getTokenPayload();
    this.updateView();
  }

  private updateView() {
    if (this.checkPermission()) {
        this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }

  private checkPermission() {
    let hasPermission = false;

    if (this.user.permissions) {
      for (const checkPermission of this.permissions) {
        hasPermission = this.user.permissions.find((element) => {
          console.log(element, checkPermission);
          return element === checkPermission; 
        });
        
      }
    }
    return hasPermission;
  }

}
